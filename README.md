# CLiMB Computing Cluster

This is the GitLab page for the CLiMB Computing Cluster.

There is a [WIKI](https://gitlab.com/levidd/climb-computing-cluster/-/wikis/home) to describe how the framework is set up, with details of each step and code involved. There is also areas to describe maintenance
and how to interact with the cluster. 


The code saved here will be the HTML and REST API made for dealing with the web interface for job scheduling and the like. It is intended to make reinstallation of 
the web interface easy, and changes made should be commited. Note, the other code for the framework and lower level portions (such as slurm installation and software)
are noted in the WIKI. The should be downloaded/installed/saved into specific areas as described in the WIKI. 

